
public class Aluno
{
    //ATRIBUTOS
    String nome;
    double nota;
    
    //CONSTRUTOR
    public Aluno(String nomeAluno, double notaAluno){
        nome = nomeAluno;
        nota = notaAluno;
    }
    public Aluno(String nomeAluno){
        nome = nomeAluno;
        nota = 0;
    }
    
    //MÉTODOS
    public boolean estaAprovado(){
        
        return (nota >= 7);
        
    }
    
    
    //MÉTODOS DE ACESSO GET(RETORNA) E SET(ALTERAR)
    //VOID NÃO TEM RETORNO
    public void setNome(String novoNome){
        if (novoNome != null && !novoNome.equals ("") ){
            nome = novoNome;
        }
    }
    public String getNome(){
        return nome;
    }
    
    public void setNota(double novaNota){
        if(novaNota >= 0 && novaNota <= 10){
            nota = novaNota;
        }
    }
    public double getNota(){
        return nota;
    }
}
